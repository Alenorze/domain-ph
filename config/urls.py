from django.contrib import admin
from django.urls import path

from keywords.views import KeywordView



urlpatterns = [
    path('', KeywordView.as_view())
]
